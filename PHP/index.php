<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="/styles/main_style.css" rel="stylesheet">
        <link href="/styles/timer.css" rel="stylesheet">
        <title>Прогноз</title>
    </head>
    <body>
        <?php
            echo "
            <p align='center'><img src='/pics/logo2.jpg' class='img-fluid'></p>
            <div class='container p-3 h-80 justify-content-center align-items-center text-center'>
                <div class='row'>
                    <div class='col-sm-6'>
                        <h3 class='countdown-title'>Следующий Гран-При</h3>
                        <p align='center'><img src='/pics/italy.jpeg'></p>
                        <p align='center'><font color='#fff'>10-12.09.2021</font></p>    
                    </div>
                    
                    
                    <!--<div class='timer' data-finish='<?=(strtotime('+1 day') * 1000)?>'>
                        <div class='timer_section'>
                            <div class='days_1'>0</div>
                            <div class='days_2'>0</div>
                            <div class='timer_section_desc'>дней</div>
                        </div>
                        <div class='timer_delimetr'>:</div>
                        <div class='timer_section'>
                            <div class='hours_1'>0</div>
                            <div class='hours_2'>0</div>
                            <div class='timer_section_desc'>часов</div>
                        </div>
                        <div class='timer_delimetr'>:</div>
                        <div class='timer_section'>
                            <div class='minutes_1'>0</div>
                            <div class='minutes_2'>0</div>
                            <div class='timer_section_desc'>минут</div>
                        </div>
                        <div class='timer_delimetr'>:</div>
                        <div class='timer_section'>
                            <div class='seconds_1'>0</div>
                            <div class='seconds_2'>0</div>
                            <div class='timer_section_desc'>секунд</div>
                        </div>
                    </div>-->

                    
                    <div class='col-sm-6'>
                        <div id='deadline-message' class='deadline-message'>
                            <p><font size='10pt' color='#cc0e0e'>Время для голосования исекло</font></p>
                            
                        </div>
                        <div id='countdown' class='countdown'>
                        <h3 class='countdown-title'>До окончания прогноза:</h3>
                            <div class='countdown-number'>
                                <span class='days countdown-time'></span>
                                <span class='countdown-text'>Д.</span>
                            </div>
                            <div class='countdown-number'>
                                <span class='hours countdown-time'></span>
                                <span class='countdown-text'>Ч.</span>
                            </div>
                            <div class='countdown-number'>
                                <span class='minutes countdown-time'></span>
                                <span class='countdown-text'>М.</span>
                            </div>
                            <div class='countdown-number'>
                                <span class='seconds countdown-time'></span>
                                <span class='countdown-text'>С.</span>
                            </div>
                            
                            <div>
                                <form method='post' action='new_forecast.php'>
                                    <br>
                                    <button type='submit' name='new_forecast' class='btn btn-success ml-auto mr-auto'><font color='#fff'>Сделать прогноз</font></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-sm-12'>
                        
                    </div>
                </div>
            </div>
            <div class='container p-3 h-80 justify-content-center align-items-center text-center'>";
                require_once 'connection.php';
                $link = mysqli_connect($host, $user, $password, $database) 
                    or die("Ошибка " . mysqli_error($link));
                mysqli_set_charset($link, "utf8");
                $query ="SELECT pilot_name,points FROM pilots ORDER BY points DESC";
                $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
            echo "
            <div class='row'>  
                <div class='col-sm-4'>
                    <p align='center'><h3><font color='#fff'>Личный зачет</font></h3></p>
                    <table class='table table-sm table-bordered border-success table-secondary table-striped'>
                    <thead class='table-info table-bordered border-success'>
                        <tr>
                            <th scope='col' width=160px, align='left'>Пилот</th>
                            <th scope='col' width=160px, align='left'>Очки</th> 
                        </tr>
                    </thead>";
            while ($row = mysqli_fetch_row($result)){
                echo "<tr>
                        <td width=160px, align='left'>$row[0]</td>
                        <td width=160px, align='left'>$row[1]</td>
                    </tr>";
                }
                echo "</table>
                    </div>";
                $query ="SELECT team_name,points FROM teams ORDER BY points DESC";
                $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
            echo "  
                <div class='col-sm-4'>
                    <p align='center'><h3><font color='#fff'>Кубок конструкторов</font></h3></p>    
                    <table class='table table-sm table-bordered border-success table-secondary table-striped'>
                    <thead class='table-info table-bordered border-success'>
                        <tr>
                            <th scope='col' width=160px, align='left'>Команда</th>
                            <th scope='col' width=160px, align='left'>Очки</th> 
                        </tr>
                    </thead>";
            while ($row = mysqli_fetch_row($result)){
                echo "<tr>
                        <td width=160px, align='left'>$row[0]</td>
                        <td width=160px, align='left'>$row[1]</td>
                    </tr>";
                }
                echo "</table>
                    </div>";
                    $query ="SELECT username,points FROM users ORDER BY points DESC";
                    $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
                echo "  
                    <div class='col-sm-4'>
                        <p align='center'><h3><font color='#fff'>Лидеры прогнозов</font></h3></p>
                        <table class='table table-sm table-bordered border-success table-secondary table-striped'>
                        <thead class='table-info table-bordered border-success'>
                            <tr>
                                <th scope='col' width=160px, align='left'>Участник</th>
                                <th scope='col' width=160px, align='left'>Очки</th> 
                            </tr>
                        </thead>";
                while ($row = mysqli_fetch_row($result)){
                    echo "<tr>
                            <td width=160px, align='left'>$row[0]</td>
                            <td width=160px, align='left'>$row[1]</td>
                        </tr>";
                    }
                    echo "</table>
                        </div>
                </div>
            </div>";
        ?>
    </body>
    <script src="/scripts/timer.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</html>