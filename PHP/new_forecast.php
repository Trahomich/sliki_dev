<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="/styles/main_style.css" rel="stylesheet">
        <title>Добавление прогноза</title>
    </head>
    <body>
        <p align='center'><img src='/pics/logo2.jpg' class='img-fluid'></p>
        <form method="post" action="new_forecast.php">
			<div class="container-fluid p-3 h-80 justify-content-center align-items-center text-center">
				<div class="row">
					<div class="col-6">
						<br>
						<br>
						<br>
						<h3 class="text-center"><font color='#3366ff'>Добавление прогноза</font></h3>
					</div>
				</div>
				<div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q1" required>
							<option disabled selected>Позиция 1</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r1" required>
                        <option disabled selected>Позиция 1</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q2" required>
							<option disabled selected>Позиция 2</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r2" required>
                        <option disabled selected>Позиция 2</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q3" required>
							<option disabled selected>Позиция 3</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r3" required>
                        <option disabled selected>Позиция 3</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q4" required>
							<option disabled selected>Позиция 4</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r4" required>
                        <option disabled selected>Позиция 4</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q5" required>
							<option disabled selected>Позиция 5</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>>
						<select name="r5" required>
                        <option disabled selected>Позиция 5</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q6" required>
							<option disabled selected>Позиция 6</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r6" required>
                        <option disabled selected>Позиция 6</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q7" required>
							<option disabled selected>Позиция 7</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>>
						<select name="r7" required>
                        <option disabled selected>Позиция 7</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q8" required>
							<option disabled selected>Позиция 8</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r8" required>
                        <option disabled selected>Позиция 8</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q9" required>
							<option disabled selected>Позиция 9</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r9" required>
                        <option disabled selected>Позиция 9</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
                <div class="row">
                    <div class='col-2'>
                    </div>
					<div class="col-4">
						<br>
						<select name="q10" required>
							<option disabled selected>Позиция 10</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
					<div class="col-4">
						<br>
						<select name="r10" required>
                        <option disabled selected>Позиция 10</option>
							<option>Lewis Hamilton (HAM)</option>
							<option>Valtteri Bottas (BOT)</option>
                            <option>Max Verstappen (VER)</option>
                            <option>Sergio Perez (PER)</option>
                            <option>Lando Norris (NOR)</option>
                            <option>Daniel Ricciardo (RIC)</option>
                            <option>Sebastian Vettel (VET)</option>
                            <option>Lance Stroll (BOT)</option>
                            <option>Fernando Alonso (ALO)</option>
                            <option>Esteban Ocon (OCO)</option>
                            <option>Charles Leclerc (LEC)</option>
                            <option>Carlos Sainz (SAI)</option>
                            <option>Pierre Gasly (GAS)</option>
                            <option>Yuki Tsunoda (TSU)</option>
                            <option>Kimi Raikkonen (RAI)</option>
                            <option>Antonio Giovinazzi (GIO)</option>
                            <option>Mick Schumacher (MSC)</option>
                            <option>Nikita Mazepin (MAZ)</option>
                            <option>George Russell (RUS)</option>
                            <option>Nicholas Latifi (LAT)</option>
                            <option>Robert Kubica (KUB)</option>
						</select>
					</div>
                    <div class='col-2'>
                    </div>
				</div>
            </div>
		</form>
        <?php
			include 'connection.php';
			$link = mysqli_connect($host, $user, $password, $database)
				or die("Ошибка " . mysqli_error($link));
			mysqli_set_charset($link, "utf8");

			if (isset($_POST["q1"]) && isset($_POST["r1"]) && isset($_POST["q2"]) && isset($_POST["r2"]) && isset($_POST["q3"]) && isset($_POST["r3"])
            && isset($_POST["q4"]) && isset($_POST["r4"]) && isset($_POST["q5"]) && isset($_POST["r5"]) && isset($_POST["q6"]) && isset($_POST["r6"])
            && isset($_POST["q7"]) && isset($_POST["r7"]) && isset($_POST["q8"]) && isset($_POST["r8"]) && isset($_POST["q9"]) && isset($_POST["r9"])
            && isset($_POST["q10"]) && isset($_POST["r10"]))
			{
				$q1 = $_POST['q1'];
				$r1 = $_POST['r1'];
                $q2 = $_POST['q2'];
				$r2 = $_POST['r2'];
                $q3 = $_POST['q3'];
				$r3 = $_POST['r3'];
                $q4 = $_POST['q4'];
				$r4 = $_POST['r4'];
                $q5 = $_POST['q5'];
				$r5 = $_POST['r5'];
                $q6 = $_POST['q6'];
				$r6 = $_POST['r6'];
                $q7 = $_POST['q7'];
				$r7 = $_POST['r7'];
                $q8 = $_POST['q8'];
				$r8 = $_POST['r8'];
                $q9 = $_POST['q9'];
				$r9 = $_POST['r9'];
                $q10 = $_POST['q10'];
				$r10 = $_POST['r10'];
				$sql = "INSERT INTO current_gp (q1,r1,q2,r2,q3,r4,q5,r5,q6,r6,q7,r7,q8,r8,q9,r9,q10,r10) 
							VALUES ('$q1','$r1','$q2','$r2','$q3','$r3','$q4','$r4','$q5','$r5','$q6','$r6','$q7','$r7','$q8','$r8','$q9','$r9','$q10','$r10')";
				if (mysqli_query($link, $sql)) {
					//echo "<p>Данные успешно добавлены в таблицу.</p>";
				} else {
					echo "<p>Произошла ошибка: " . mysqli_error($link) . "</p>";
				}
			}
		?>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</html>