<html>
    <head>
        <title>Прогноз (Авторизация)</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/styles/login.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="tab" role="tabpanel">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Авторизация</a></li>
                        </ul>

                        <div class="tab-content tabs">
                            <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                                <form action="log_in.php" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Имя пользователя</label>
                                        <input type="text" class="form-control" name="username" id="exampleInputLogin1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Пароль</label>
                                        <input type="password" class="form-control" name="userpassword" id="exampleInputPassword1">
                                    </div>
                                    <!--<div class="form-group">
                                        <div class="main-checkbox">
                                            <input value="None" id="checkbox1" name="check" type="checkbox">
                                            <label for="checkbox1"></label>
                                        </div>
                                        <span class="text">Оставаться в системе</span>
                                    </div>-->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default" name="auth">Авторизоваться</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            session_start();
            /*if($_SESSION['admin']){
                header("Location: admin.php");
                exit;
            }*/
            if(isset($_POST['auth'])){
                require_once 'connection.php';
                $link = mysqli_connect($host, $user, $password, $database) 
                    or die("Ошибка " . mysqli_error($link));
                mysqli_set_charset($link, "utf8");
                $query ="SELECT username,hash FROM users";
                $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
                if($result){
                    $row = mysqli_fetch_row($result);
                    $login = $row[0];
                    $pass = $row[1];
                    if($login == $_POST['username'] AND $pass == md5($_POST['userpassword'])){
                        $_SESSION['user'] = $login;
                        header("Location: index.php");
                        exit;
                    }
                    else {
                        echo '<script>
                            alert("Логин или пароль неверные!");
                        </script>';
                    }
                    mysqli_free_result($result);
                }
                mysqli_close($link);
            }
        ?>
    </body>
</html>