docker service rm sliki-dev 



docker service  create  --name=sliki-dev  -w /app \
--with-registry-auth --replicas=1 \
--constraint node.role==manager \
--add-host sliki.oblaka.pw:192.168.99.10
--network=traefik-public \
    --label="traefik.enable=true" \
    --label="traefik.backend.loadbalancer.swarm=true" \
    --label="traefik.http.services.sliki.loadbalancer.server.port=3000" \
    --label="traefik.backend.loadbalancer.healthCheck.path=\"/health\"" \
    --label="traefik.backend.loadbalancer.healthCheck.interval=10s" \
    --label="traefik.backend.loadbalancer.healthChecktimeout=3s" \
    --label="traefik.http.routers.slikidev.rule=Host(\"sliki.oblaka.pw\")" \
    --label="traefik.http.routers.slikidev.entrypoints=websecure" \
    --label="traefik.http.routers.slikidev.tls.certresolver=letsencryptresolver" \
 sliki_dev  npm run start
