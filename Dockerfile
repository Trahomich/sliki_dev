# DEV version
FROM node:lts-alpine
RUN mkdir /app
COPY . /app

RUN cd /app && npm i

# build necessary, even if no static files are needed,
# since it builds the server as well
RUN cd /app && npm run build

# expose 3000 on container
EXPOSE 3000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=3000

# start the app
CMD [ "npm", "start" ]



